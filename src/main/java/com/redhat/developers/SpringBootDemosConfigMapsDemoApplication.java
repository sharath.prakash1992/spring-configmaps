package com.redhat.developers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootDemosConfigMapsDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDemosConfigMapsDemoApplication.class, args);
	}
}
